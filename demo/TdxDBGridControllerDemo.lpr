Program TdxDBGridControllerDemo;

{$mode objfpc}{$H+}

Uses
   {$IFDEF UNIX}
  cthreads,
   {$ENDIF}
   {$IFDEF HASAMIGA}
  athreads,
   {$ENDIF}
   Forms,
   Interfaces, // this includes the LCL widgetset
   MainFormDemo { you can add units after this };

   {$R *.res}

Begin
   RequireDerivedFormResource := True;
   Application.Scaled         := True;
   Application.Initialize;
   Application.CreateForm(TfmMainDemo, fmMainDemo);
   Application.Run;
End.
